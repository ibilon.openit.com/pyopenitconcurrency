# pyopenitconcurrency

A general-purpose concurrency algorithm library in Python.

## Install

```
pip install git+https://gitlab.com/ibilon.openit.com/pyopenitconcurrency.git#egg=openitconcurrency
```

## Example

```python
from openitconcurrency import concurrency
import pandas

dataframe = pandas.DataFrame({
    'name' : ['A', 'A', 'B'],
    'start': [
        pandas.Timestamp('2022-01-01 01:05'),
        pandas.Timestamp('2022-01-01 01:10'),
        pandas.Timestamp('2022-01-01 01:00'),
    ],
    'duration': [
        pandas.Timedelta(10, unit='minute'),
        pandas.Timedelta(10, unit='minute'),
        pandas.Timedelta(5, unit='minute'),
    ],
    'count': [1, 2, 3]
})
```

```python
print(dataframe)
```

```
  name               start        duration  count
0    A 2022-01-01 01:05:00 0 days 00:10:00      1
1    A 2022-01-01 01:10:00 0 days 00:10:00      2
2    B 2022-01-01 01:00:00 0 days 00:05:00      3
```

```python
print(concurrency(dataframe))
```

```
  name                time  count        duration
0    A 2022-01-01 01:00:00      0 0 days 00:45:00
1    A 2022-01-01 01:00:00      1 0 days 00:05:00
2    A 2022-01-01 01:00:00      2 0 days 00:05:00
3    A 2022-01-01 01:00:00      3 0 days 00:05:00
4    B 2022-01-01 01:00:00      0 0 days 00:55:00
5    B 2022-01-01 01:00:00      3 0 days 00:05:00
```

## Limitation

All data's start and duration must be inside the timeframe.
If not, you need to prepare and slice the data first.

## Timeframe

The default timeframe is hourly. To change the timeframe, specify the `freq` parameter. For example:

```python
concurrency(dataframe, freq='H')
```

See the list of possible values [here](https://pandas.pydata.org/docs/user_guide/timeseries.html#timeseries-offset-aliases).
