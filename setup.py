from setuptools import setup, find_packages

setup(
    name='openitconcurrency',
    version='0.5.0',
    packages=find_packages(),
    install_requires=[
        'pandas'
    ]
)
