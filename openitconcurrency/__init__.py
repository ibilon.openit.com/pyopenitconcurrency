from pandas.tseries.offsets import DateOffset
from pandas import DataFrame, concat

def concurrency(dataframe, freq='H'):
    dataframe['time'] = dataframe['start'].dt.floor(freq)
    dataframe['next'] = (dataframe['time'] + DateOffset(seconds=1)).dt.ceil(freq)
    dataframe['end']  = dataframe['start'] + dataframe['duration']
    groups = dataframe.groupby('name')
    units  = [ [ { 'name': name, 'time': time, 'dataframe': unit } for time, unit in group.groupby(by='time') ] for name, group in groups ]
    units  = map_unit('points', get_points, units)
    units  = map_unit('collect', collect, units)
    return sum(to_dataframe(units))

def map_unit(name, function, units):
    for group in units:
        for unit in group:
            unit[name] = function(unit)
    return units

def get_points(unit):
    dataframe = unit['dataframe']
    points    = concat([ dataframe['time'], dataframe['start'], dataframe['end'], dataframe['next'] ]).sort_values().unique()
    return [ (points[i], points[i+1]) for i in range(len(points) - 1) ]

def collect(unit):
    dataframe = unit['dataframe']
    points    = unit['points']
    duration  = [ point[1] - point[0] for point in points ]
    count     = [ dataframe['count'][ (dataframe['start'] <= point[0]) & (dataframe['end'] >= point[1]) ].sum() for point in points ]
    return count, duration

def to_dataframe(units):
    table = { 'name': [], 'time': [], 'count': [], 'duration': [] }
    for group in units:
        for unit in group:
            count, duration    = unit['collect']
            records            = len(count)
            table['name']     += [ unit['name'] ] * records
            table['time']     += [ unit['time'] ] * records
            table['count']    += count
            table['duration'] += duration
    return DataFrame(table)

def sum(dataframe):
    return dataframe.groupby(by=['name', 'time', 'count']).sum().reset_index()
